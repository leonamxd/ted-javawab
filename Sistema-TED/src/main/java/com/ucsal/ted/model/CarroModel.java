package com.ucsal.ted.model;

public class CarroModel {

	private String nome;
	private String modelo;
	private int ano;
	
	
	public CarroModel() {
		
	}
	
	
	public CarroModel(String nome, String modelo, int ano) {
		super();
		this.nome = nome;
		this.modelo = modelo;
		this.ano = ano;
	}
	
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	
	
	
	
	
	
}
