package com.ucsal.ted.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ucsal.ted.model.CarroModel;

@Controller
public class TedController {

	@GetMapping({"/olaMundo"})
	public String olaMundo() {
		
		return "hello";
	}
	
	
	@GetMapping({"/carro/cadastrar"})
	public String exibeCadastro() {
		return "CadastroCarro";
	}
	
	
	@RequestMapping({"/carro/novoCarro"})
	public String cadastroNovoCarro(HttpServletRequest request, 
			@RequestParam (value="nomeCarro") String nmCarro, 
			@RequestParam (value="anoCarro") String ano,
			@RequestParam (value="modeloCarro") String modelo) {
		
		int anoInt = Integer.parseInt(ano);
		ArrayList<CarroModel> arrayCarro = null;
			
		CarroModel nvCarro = new CarroModel(nmCarro,modelo,anoInt);
		
		
		
		HttpSession sessao =  request.getSession();
		
		if(sessao.getAttribute("listaCarro") == null) {
			arrayCarro = new ArrayList<CarroModel>();
			arrayCarro.add(nvCarro);
			sessao.setAttribute("listaCarro", arrayCarro);
		} else {
			
			arrayCarro = (ArrayList<CarroModel>)sessao.getAttribute("listaCarro");
			arrayCarro.add(nvCarro);
			sessao.setAttribute("listaCarro", arrayCarro);
			
		}
		
				
		
		return "CarroCadastrado";
		
		
	}
	
	
	
}
